#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <EventLoop/Algorithm.h>
#include "xAODTruth/TruthParticle.h"
#include "xAODTracking/TrackParticle.h"

//#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>

#include "xAODParticleEvent/CompositeParticle.h"
#include "xAODParticleEvent/CompositeParticleContainer.h"
#include "xAODParticleEvent/CompositeParticleAuxContainer.h"
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/DiTauJetContainer.h"

// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>

#include <TH1.h>
#include <TTree.h>
#include <vector>
#include <TLorentzVector.h>
#include <TVector3.h>

class MyxAODAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

  int m_pid = 25;
  int m_status = 62;
  int m_tau_status = 2;
  int m_cut_ncomb = 3;
  int m_cut_nsoft = 3;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!


  int m_eventCounter; //!
  int m_trackCounter; //!

  std::string m_outputName;
  TTree *m_tree; //!

  unsigned long long r_evtNumber; //!
  uint32_t r_runNumber; //!
  uint32_t r_lumiBlock; //!
  int r_npv; //!
  int r_ntrk; //!
  int r_nmu; //!
  float r_privx_mu; //!
  Char_t r_period; //!
  Char_t r_stream; //!

  float r_X_mass; //!
  float r_X_pT; //!
  float r_phistar_mc; //!
  float r_phistar_rec_mc; //!
  float r_phistar_rec; //!
  float r_phistar_rec_res; //!

  float r_theta_mc; //!
  float r_theta_rec; //!

  float r_xMC; //!
  float r_yMC; //!
  float r_zMC; //!
  float r_rxyMC; //!
  float r_xBS; //!
  float r_yBS; //!
  float r_zBS; //!
  float r_rxyBS; //!
  float r_xnoBS; //!
  float r_ynoBS; //!
  float r_znoBS; //!
  float r_rxynoBS; //!
  float r_xcorBS; //!
  float r_ycorBS; //!
  float r_zcorBS; //!
  float r_rxycorBS; //!


  // MuonSelectionTool
  asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelection; //!

  // MuonCalibrationAndSmearing
  asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool; //!

  // GRL
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //!
  std::vector<std::string> grl_files;
  bool useGRL=true;
  // trigger tools
  asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool; //!
  asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool; //!

  // this is a standard constructor
  MyxAODAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  TVector3 pca(TVector3& sv, TVector3& pv, TVector3& mom);
  float cosTheta(TVector3& sv, TVector3& pv, TVector3& mom);
  float cosTheta_xy(TVector3& sv, TVector3& pv, TVector3& mom);
  float a0xy(TVector3& sv, TVector3& pv, TVector3& mom);
  float a0(TVector3& sv, TVector3& pv, TVector3& mom);
  float a0z(TVector3& sv, TVector3& pv, TVector3& mom);

  double deltaR(const xAOD::TruthParticle* truth, const xAOD::TrackParticle* trk);
  double deltaR(const xAOD::TruthParticle* truth, TVector3& mom);
  int index_deltaR(const xAOD::TruthParticle* truth, std::vector<const xAOD::Vertex*>& selectedParticles, double& min_dR);

  StatusCode applySelection(const xAOD::VertexContainer* inParticles, std::vector<const xAOD::Vertex*>& selectedParticles);
  bool select(const xAOD::Vertex* ptl); // choose whether to select an individual particle
  const xAOD::Vertex* findPV(unsigned int i_onia, const xAOD::VertexContainer* pvs);
  bool trackInPV(const xAOD::TrackParticle* TP, const xAOD::Vertex* pv);
  bool passIP(const xAOD::TrackParticle* TP, const xAOD::Vertex* pv, const xAOD::EventInfo *eventInfo);
  int selectVert(std::vector<const xAOD::Vertex*> V);
  const xAOD::TruthParticle* mc_daughter(const xAOD::TruthParticle* parent);
  void findAllDescendants(const xAOD::TruthParticle*, std::vector<const xAOD::TruthParticle*> &);

  std::vector<TVector3> nuMomentum2(double VisPx1, double VisPy1, double VisPz1, double VisE1, double deltaX, double deltaY, double deltaZ, double constraintMass1, double nuMass);

  TVector3 calculateImpactParamVec(const xAOD::TruthParticle* pion, const xAOD::TruthVertex* primaryVertex);
  TVector3 calculateImpactParamVec(const xAOD::TrackParticle* track, const TVector3& primaryVertex);
  TVector3 getIP(TLorentzVector vis, const xAOD::TruthParticle* pi, const xAOD::TruthVertex* primaryVertex);
  float Acoplanarity_IP(TLorentzVector piPlus, TLorentzVector ipVectorPlus, TLorentzVector piMinus, TLorentzVector ipVectorMinus, const TLorentzVector& referenceFrame);
  float Acoplanarity_Rho_Rho(TLorentzVector chargedPlus, TLorentzVector neutralPlus, TLorentzVector chargedMinus, TLorentzVector neutralMinus, const TLorentzVector& referenceFrame);
  float Acoplanarity_IP_rho(TLorentzVector track, TLorentzVector ipVector_track, TLorentzVector charged_rho, TLorentzVector neutral_rho, const TLorentzVector& referenceFrame, bool plus_makes_rho);
  float Upsilon(TLorentzVector charged, TLorentzVector neutral, const TLorentzVector& referenceFrame);

  //TVector3 ip_xyz3D(const xAOD::TrackParticle* track, const xAOD::Vertex* vertex);
  //TVector3 trk_pca(const xAOD::TrackParticle* track, const xAOD::Vertex* vertex);
  TVector3 ip_xyz3D(const xAOD::TrackParticle* track, TVector3 vertex);
  TVector3 trk_pca(const xAOD::TrackParticle* track, TVector3 vertex);
  TVector3 mc_pca(TLorentzVector vis, TVector3 pv, TVector3 sv, TVector3 bs);
  TVector3 linear_pca_xyz3D(TLorentzVector vis, TVector3 pv, TVector3 sv, TVector3 bs);
  float acoplanarity_IP(TLorentzVector tlv_vis1, TLorentzVector tlv_vis2, TVector3 PV_xyz, TVector3 vis1_xyz, TVector3 vis2_xyz);
  TLorentzVector dgieuler( TLorentzVector parent, TLorentzVector daughter);
  TLorentzVector dgloren( TLorentzVector p, float b, float g, float ikey);


  unsigned long m_counter_eventsConsidered, m_counter_eventsSelected, m_counter_candidatesConsidered, m_counter_candidatesSelected;
  unsigned long m_counter_no2trkVert, m_counter_inconsistent_charges, m_counter_eventsRejected;

//  ToolHandle<Trig::TrigDecisionTool> m_tdt;  //this is a data member
//  std::vector<std::string> m_mytriggers;

//  //template<typename XXX>
//  //int GetIndex(const XXX* myitem, const std::vector<XXX> &collection)
//  int GetIndex(const xAOD::Muon* myitem, const xAOD::MuonContainer* &collection)
//  {
//     auto itr = std::find(collection.begin(), collection.end(), myitem);
//     if(itr==collection.end()) return -1;
//     return std::distance(collection.begin(), *itr);
//  }


  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODAnalysis, 1);
};

#endif




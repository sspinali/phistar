#include <EventLoop/DirectDriver.h>
#include <EventLoop/Job.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>
#include <xAODRootAccess/Init.h>
#include <AsgTools/MessageCheck.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>
#include <SampleHandler/ToolsDiscovery.h>
#include <EventLoopGrid/PrunDriver.h>

void ATestSubmit (const std::string& submitDir)
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.
  
  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //const char* inputFilePath = gSystem->ExpandPathName ("$ALRB_TutorialData/r9315/");
  //SH::ScanDir().filePattern("AOD.11182705._000001.pool.root.1").scan(sh,inputFilePath);
  //const char* inputFilePath = gSystem->ExpandPathName ("/home/atlas/ab/Public/");
  ////SH::ScanDir().filePattern("DAOD_BPHY16.v3.pool.root").scan(sh,inputFilePath);
  //SH::ScanDir().filePattern("DAOD_BPHY16.bothCharge.pool.root").scan(sh,inputFilePath);
////  const char* inputFilePath = gSystem->ExpandPathName ("/scratch/eva/test/run/");
////  SH::ScanDir().filePattern("DAOD_BPHY16.v5.root").scan(sh,inputFilePath);
//  const char* inputFilePath = gSystem->ExpandPathName ("/home/atlas/eva/jobs_3mu1/job_ggpvv_00001/");
//  SH::ScanDir().filePattern("DAOD_BPHY16.ggpvv_00001.root").scan(sh,inputFilePath);
  //const char* inputFilePath = gSystem->ExpandPathName ("/io7data/eva/dionia/mc/new/DAODs/S4b18p4NP01/");
  //SH::ScanDir().filePattern("DAOD_BPHY16.S4b18p4NP01_*.root").scan(sh,inputFilePath);

//  const char* inputFilePath = gSystem->ExpandPathName ("/io7data/eva/dionia/mc/new/DAODs/S4b18p4NP01ptj09p2/");
//  SH::ScanDir().filePattern("DAOD_BPHY16.S4b18p4NP01ptj09p2_*.root").scan(sh,inputFilePath);
  //const char* inputFilePath = gSystem->ExpandPathName ("/io7data/eva/dionia/mc/new/DAODs/data17_main/data17_13TeV.00340453.physics_Main.deriv.DAOD_BPHY16.r10426_p3399_p3601_tid15173567_00/");
  //const char* inputFilePath = gSystem->ExpandPathName ("/io7data/eva/dionia/mc/new/DAODs/data17_main/data17_13TeV.00340453.physics_Main.deriv.DAOD_BPHY16.r10426_p3399_p3601/");
  //SH::ScanDir().filePattern("DAOD_BPHY16*.root*").scan(sh,inputFilePath);
  // run on the grid
  SH::scanRucio(sh, "data12_8TeV.periodAllYear.physics_Bphysics.PhysCont.DAOD_BPHY16.grp12_v01_p3810");
  //SH::scanRucio(sh, "data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_BPHY16.grp17_v01_p3704");
  ////SH::scanRucio(sh, "data17_13TeV.periodAllYear.physics_BphysLS.PhysCont.DAOD_BPHY16.grp17_v01_p3601");
  //SH::scanRucio(sh, "data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_BPHY16.grp17_v01_p3601");

  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  job.options()->setDouble (EL::Job::optMaxEvents, -1); // for testing purposes, limit to run over the first 500 events only!
  //job.options()->setDouble (EL::Job::optMaxEvents, 100); // for testing purposes, limit to run over the first 500 events only!
  //job.options()->setDouble (EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!

  // add our algorithm to the job
  MyxAODAnalysis *alg = new MyxAODAnalysis;
  
  alg->useGRL = false;
  //alg->useGRL = true;
  //alg->grl_files.push_back("GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml");
  alg->grl_files.push_back("data12_8TeV.periodAllYear_DetStatus-v61-pro14-02_DQDefects-00-01-00_PHYS_StandardGRL_All_Good.xml");

  // set the name of the algorithm (this is the name use with
  // messages)
  alg->SetName ("AnalysisAlg");

  // define an output and an ntuple associated to that output
  EL::OutputStream output  ("myOutput");
  job.outputAdd (output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
  job.algsAdd (ntuple);


  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (alg);

  alg->m_outputName = "myOutput"; // give the name of the output to our algorithm

  //alg->m_cut_mXmax = 50.0;              // default 100.

  // make the driver we want to use:
  // this one works by running the algorithm directly:
//  EL::DirectDriver driver;
  // we can use other drivers to run things on the Grid, with PROOF, etc.
  // process the job using the driver
//  driver.submit (job, submitDir);

  // run on grid
  EL::PrunDriver driver;
  driver.options()->setString( "nc_outputSampleName" ,  "user.bouhova.data12_8TeV.periodAllYear.physics_Bphysics.PhysCont.DAOD_BPHY16.grp12_v01_p3810_4mu_pref.v1");
  //driver.options()->setString( "nc_outputSampleName" ,  "user.bouhova.data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_BPHY16.grp17_v01_p3704_4mu_pref.v1");
  ////driver.options()->setString( "nc_outputSampleName" ,  "user.bouhova.data17_13TeV.periodAllYear.physics_BphysLS.PhysCont.DAOD_BPHY16.grp17_v01_p3601_191018.v2_new");
  //driver.options()->setString( "nc_outputSampleName" ,  "user.bouhova.data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_BPHY16.grp17_v01_p3601_191018.v3");
  driver.submitOnly (job, submitDir);

}


int main (int argc, char* argv[])
{
  // set the return type and reporting category for ANA_CHECK
  using namespace asg::msgUserCode;
  ANA_CHECK_SET_TYPE (int);

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc == 2 ) submitDir = argv[1];
  if (argc > 2)
  {
    ANA_MSG_ERROR ("don't know what to do with extra arguments, aborting");
    return -1;
  }

  // Set up the job for xAOD access:
  ANA_CHECK (xAOD::Init());

  // call our actual job-submission code
  ATestSubmit (submitDir);

  return 0;
}



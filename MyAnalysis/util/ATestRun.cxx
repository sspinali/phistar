#include <EventLoop/DirectDriver.h>
#include <EventLoop/Job.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>
#include <xAODRootAccess/Init.h>
#include <AsgTools/MessageCheck.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>

void ATestRun (const std::string& submitDir)
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.
  
  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //const char* inputFilePath = gSystem->ExpandPathName ("$ALRB_TutorialData/r9315/");
  //SH::ScanDir().filePattern("AOD.11182705._000001.pool.root.1").scan(sh,inputFilePath);

  //const char* inputFilePath = gSystem->ExpandPathName ("/home/atlas/eva/tau_der/run/");
  //SH::ScanDir().filePattern("DAOD_HIGG4D3.test1.pool.root").scan(sh,inputFilePath);
  //const char* inputFilePath = gSystem->ExpandPathName ("/data/eva/htautau/htautau_DAOD/mc16_13TeV.346563.PowhegPy8EG_NNLOPS_nnlo30_ggH125_tautauh30h20_mix50.deriv.DAOD_HIGG4D2.e7610_s3126_r10724_p3978");
  //const char* inputFilePath = gSystem->ExpandPathName ("/data/eva/htautau/htautau_DAOD/mc16_13TeV.346562.PowhegPy8EG_NNLOPS_nnlo30_ggH125_tautauh30h20_CPodd.deriv.DAOD_HIGG4D2.e7610_s3126_r10724_p3978");
  //const char* inputFilePath = gSystem->ExpandPathName ("/data/eva/htautau/htautau_DAOD/mc16_13TeV.346564.PowhegPy8EG_NNLOPS_nnlo30_ggH125_tautauh30h20_unpol.deriv.DAOD_HIGG4D2.e7610_s3126_r10724_p3978");
 // const char* inputFilePath = gSystem->ExpandPathName ("/data/eva/htautau/htautau_DAOD/mc16_13TeV.346193.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautauh30h20.deriv.DAOD_HIGG4D2.e7259_s3126_r10724_p3978");
  ///const char* inputFilePath = gSystem->ExpandPathName ("/data/eva/htautau/htautau_AOD/mc16_13TeV.346193.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautauh30h20.merge.AOD.e7259_e5984_s3126_r10724_r10726");
  const char* inputFilePath = gSystem->ExpandPathName ("/data/eva/htautau/htautau_AOD/mc16_13TeV.346562.PowhegPy8EG_NNLOPS_nnlo30_ggH125_tautauh30h20_CPodd.merge.AOD.e7610_e5984_s3126_r10724_r10726");
  //SH::ScanDir().filePattern("DAOD_HIGG4D2.19346011._000001.pool.root.1").scan(sh,inputFilePath);
  //const char* inputFilePath = gSystem->ExpandPathName ("/data/eva/htautau/htautau_DAOD/mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_HIGG4D2.e3601_s3126_r10724_p3978");
  //const char* inputFilePath = gSystem->ExpandPathName ("/data/eva/htautau/htautau_DAOD/mc16_13TeV.346562.PowhegPy8EG_NNLOPS_nnlo30_ggH125_tautauh30h20_CPodd.deriv.DAOD_HIGG4D2.e7610_s3126_r10724_p3978");
  //SH::ScanDir().filePattern("DAOD_HIGG4D2.19354571._000046.pool.root.1").scan(sh,inputFilePath);
  //SH::ScanDir().filePattern("DAOD_HIGG4D2.19346011._000003.pool.root.1").scan(sh,inputFilePath);
 // SH::ScanDir().filePattern("DAOD_HIGG4D2*.pool.root*").scan(sh,inputFilePath);
  SH::ScanDir().filePattern("AOD*.pool.root*").scan(sh,inputFilePath);
//  const char* inputFilePath = gSystem->ExpandPathName ("/home/atlas/eva/user.pwagner.345123.ggH125_tautauh30h20.r10201.vtxRefit.v001e_EXT0/");
//  SH::ScanDir().filePattern("user.pwagner.19649115.EXT0._000003.DAOD_HIGG4D3.test.pool.root").scan(sh,inputFilePath);
  //const char* inputFilePath = gSystem->ExpandPathName ("/data/eva/htautau/htautau_DAOD/mc16_13TeV.346193.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautauh30h20.deriv.DAOD_HIGG4D2.e7259_s3126_r10724_p3978/");
  //SH::ScanDir().filePattern("DAOD_HIGG4D2.19346011._000003.pool.root.1").scan(sh,inputFilePath);
  //const char* inputFilePath = gSystem->ExpandPathName ("/data/eva/htautau/htautau_AOD/mc16_13TeV.346193.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautauh30h20.merge.AOD.e7259_e5984_s3126_r10724_r10726/");
  //SH::ScanDir().filePattern("AOD.17070081._000001.pool.root.1").scan(sh,inputFilePath);
//  const char* inputFilePath = gSystem->ExpandPathName ("/atlas_data/awharton/ForMatt/mc16_13TeV.345835.aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_ttbb_hh.deriv.DAOD_HIGG4D3.e7106_s3126_r10724_p3749/");
//  SH::ScanDir().filePattern("DAOD_HIGG4D3.16780302._000017.pool.root.1").scan(sh,inputFilePath);
  //const char* inputFilePath = gSystem->ExpandPathName ("/atlas_data/awharton/ForMatt/mc16_13TeV.345835.aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_ttbb_hh.recon.AOD.e7106_s3126_r10724/");
  //SH::ScanDir().filePattern("AOD.16476389._000009.pool.root.1").scan(sh,inputFilePath);


  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  job.options()->setDouble (EL::Job::optMaxEvents, -1); // for testing purposes, limit to run over the first 500 events only!
  //job.options()->setDouble (EL::Job::optMaxEvents, 50); // for testing purposes, limit to run over the first 500 events only!
  //job.options()->setDouble (EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!

  // add our algorithm to the job
  MyxAODAnalysis *alg = new MyxAODAnalysis;

  //alg->useGRL = true;
  alg->useGRL = false;
  alg->grl_files.push_back("GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml");
  //alg->grl_files.push_back("data12_8TeV.periodAllYear_DetStatus-v61-pro14-02_DQDefects-00-01-00_PHYS_StandardGRL_All_Good.xml");

  // set the name of the algorithm (this is the name use with
  // messages)
  alg->SetName ("AnalysisAlg");

  // define an output and an ntuple associated to that output
  EL::OutputStream output  ("myOutput");
  job.outputAdd (output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
  job.algsAdd (ntuple);


  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (alg);

  alg->m_outputName = "myOutput"; // give the name of the output to our algorithm

  //alg->m_pid = 23;                        // default 25
  //alg->m_status = 11;                     // default 62
  //alg->m_cut_mXmax = 50.0;              // default 100.


  // make the driver we want to use:
  // this one works by running the algorithm directly:
  EL::DirectDriver driver;
  // we can use other drivers to run things on the Grid, with PROOF, etc.

  // process the job using the driver
  driver.submit (job, submitDir);
}


int main (int argc, char* argv[])
{
  // set the return type and reporting category for ANA_CHECK
  using namespace asg::msgUserCode;
  ANA_CHECK_SET_TYPE (int);

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc == 2 ) submitDir = argv[1];
  if (argc > 2)
  {
    ANA_MSG_ERROR ("don't know what to do with extra arguments, aborting");
    return -1;
  }

  // Set up the job for xAOD access:
  ANA_CHECK (xAOD::Init());

  // call our actual job-submission code
  ATestRun (submitDir);

  return 0;
}


